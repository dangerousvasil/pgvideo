<?php

namespace app\models;

/**
 * This is the model class for table "video".
 *
 * @property int $id
 * @property string $title
 * @property string $file
 * @property string $thumbnail
 * @property string $added
 * @property int $duration
 * @property int $views
 */
class Video extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'video';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'file', 'thumbnail', 'added', 'duration', 'views'], 'required'],
            [['added'], 'safe'],
            [['duration', 'views'], 'default', 'value' => 0],
            [['duration', 'views'], 'integer'],
            [['title', 'file', 'thumbnail'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'file' => 'File',
            'thumbnail' => 'Thumbnail',
            'added' => 'Added',
            'duration' => 'Duration',
            'views' => 'Views',
        ];
    }

    /**
     * {@inheritdoc}
     * @return VideoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VideoQuery(get_called_class());
    }

    public function delete()
    {
        return $this->delete();
    }
}
