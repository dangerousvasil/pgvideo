<?php


namespace app\widgets;


use Yii;
use yii\bootstrap\Dropdown;
use yii\bootstrap\Html;
use yii\bootstrap\Widget;
use yii\web\Request;

/**
 * Drop down menu for default pagination page size parameter.
 *
 * Class PageSizeWidget
 * @package app\widgets
 */
class PageSizeWidget extends Widget
{
    /**
     * @var string name of the parameter storing the page size.
     */
    public $name = 'per-page';
    /**
     * @var string[] array of elements can be selected
     */
    public $elements = [
        4 => '4 items',
        12 => '12 items',
        16 => '16 items',
        20 => '20 items',
        24 => '24 items',
        28 => '28 items',
        32 => '32 items',
        36 => '36 items',
        40 => '40 items'
    ];
    /**
     * @var null
     */
    protected $params = null;
    /**
     * @var mixed
     */
    public $defaultPageSize = 20;
    /**
     * @var string name of the parameter storing the page size.
     */
    public $pageSizeParam = 'per-page';
    /**
     * @var string default Label for dropdown menu
     */
    public $label = 'Items per page ({count}) {caret}';
    /**
     * @var int Current page size parameter
     */
    private $_pageSize;

    /**
     * Runs the widget.
     * @return string
     * @throws \Exception
     */
    public function run()
    {
        $items = [];
        foreach ($this->elements as $value => $label) {
            $items[] = ['label' => $label, 'url' => $this->createUrl($value)];
        }
        return Html::tag('div',
            Html::a($this->getLabel(), '#', ['class' => "dropdown-toggle", 'data-toggle' => "dropdown"]) .
            Dropdown::widget(['items' => $items])
            , ['class' => 'dropdown']);
    }

    /**
     * Creates a relative URL using the query parameters.
     * @param $value
     * @return string
     */
    protected function createUrl($value)
    {
        $params = $this->getAllParams();
        $params[$this->name] = $value;
        return Yii::$app->getUrlManager()->createUrl($params);
    }

    /**
     * Returns the values of the query parameters.
     * @return array assoc array of parameters and values
     */
    protected function getAllParams()
    {
        if ($this->params === null) {
            $request = Yii::$app->getRequest();
            $this->params = $request instanceof Request ? $request->getQueryParams() : [];
        }
        return $this->params;
    }


    /**
     * Returns the zero-based current pagesize number.
     * @param bool $recalculate whether to recalculate the current page based on the page size and item count.
     * @return int the zero-based current page number.
     */
    protected function getPageSize($recalculate = false)
    {
        if ($this->_pageSize === null || $recalculate) {
            $page = (int)$this->getQueryParam($this->pageSizeParam, $this->defaultPageSize);
            $this->_pageSize = $page;
        }

        return $this->_pageSize;
    }

    /**
     * Returns the value of the specified query parameter.
     * This method returns the named parameter value from [[params]]. Null is returned if the value does not exist.
     * @param string $name the parameter name
     * @param string $defaultValue the value to be returned when the specified parameter does not exist in [[params]].
     * @return string the parameter value
     */
    protected function getQueryParam($name, $defaultValue = null)
    {
        $params = $this->getAllParams();
        return isset($params[$name]) && is_scalar($params[$name]) ? $params[$name] : $defaultValue;
    }

    /**
     * Returns the label string can replace {count} and {caret} placeholders.
     * @return string the label value
     */
    protected function getLabel(): string
    {
        $lbl = $this->label;
        $lbl = str_replace('{count}', $this->getPageSize(), $lbl);
        $lbl = str_replace('{caret}', Html::tag('b', '', ['class' => "caret"]), $lbl);

        return $lbl;
    }

}