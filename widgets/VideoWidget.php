<?php


namespace app\widgets;


use app\models\Video;
use Yii;
use yii\bootstrap\Html;
use yii\bootstrap\Widget;
use yii\helpers\Inflector;

class VideoWidget extends Widget
{
    /**
     * @var Video
     */
    public $model = null;

    /**
     * Thumb image size
     * @var int
     */
    public $thumbImgSize = 150;

    /**
     * Runs widget.
     * @return string
     */
    public function run()
    {
        return $this->getHeaderBlock() .
            $this->getThumbBlock() .
            $this->getFooterBlock();
    }

    /**
     * Header block with title and url
     * @return string
     */
    protected function getHeaderBlock()
    {
        return Html::tag('div',
            Html::a(Html::encode($this->model->title), ['view', 'id' => $this->model->id]));
    }

    /**
     * Middle block with image
     * @return string
     */
    protected function getThumbBlock()
    {
        return Html::tag('div',
            Html::img($this->getThumbUrl(), ['height' => $this->thumbImgSize, 'wigth' => $this->thumbImgSize, 'style' => 'border-radius:35%;margin: 0.32em auto;display:block']) .
            Html::tag('span', Yii::$app->formatter->asDuration($this->model->duration), ['style' => 'position:absolute;bottom:0;right:2em'])
            , ['style' => 'margin:1em;position:relative;text-align:right;']
        );
    }

    /**
     * Generate random ThumbUrl with for adorable service
     * @return string
     */
    protected function getThumbUrl()
    {
        return 'https://api.adorable.io/avatars/' . $this->thumbImgSize . '/' . Inflector::slug(substr($this->model->thumbnail, 0, 100), '-') . '.png';
    }

    /**
     * Footer for video block
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    protected function getFooterBlock()
    {
        return Html::tag('div',
            Html::tag('div', Yii::$app->formatter->asDate($this->model->added), ['style' => 'float:left;']) .
            Html::tag('div', Yii::$app->formatter->asInteger($this->model->views), ['style' => 'float:right;'])
        );
    }
}