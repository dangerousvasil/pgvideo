<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Video */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Videos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
echo Html::tag('div',
Html::tag('h1', Html::encode($this->title)) .
    Html::tag('p',
        Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) .
        Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) .
        Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ])
        , ['class' => 'btn-group col-lg-12', 'role' => 'group',]) .
    Html::tag('div',
        DetailView::widget([
            'model' => $model,
            'attributes' => [
                'title',
                'file',
                'thumbnail',
                'added',
                'duration',
                'views',
            ],
        ]), ['class' => 'col-lg-12'])
    , ['class' => 'video-view']);
