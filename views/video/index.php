<?php

use app\widgets\PageSizeWidget;
use app\widgets\VideoWidget;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\widgets\LinkSorter;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Videos');

echo Html::tag('div',
    Html::tag('h1', Html::encode($this->title)) .
    ListView::widget(array(
        'dataProvider' => $dataProvider,
        'layout' =>
            Html::tag('div',
                Html::tag('div', '{sorter}', ['class' => 'col-lg-3', 'style' => 'padding:1em']) .
                Html::tag('div', '{pager}', ['class' => 'col-lg-6', 'style' => 'text-align:center']) .
                Html::tag('div', PageSizeWidget::widget(['options' => []]), ['class' => 'col-lg-3', 'style' => 'padding:1em'])
                , ['class' => 'row']) .
            Html::tag('div', '{items}', ['class' => 'row']) .
            Html::tag('div', '{pager}', ['class' => 'row', 'style' => 'text-align:center']),
        'itemOptions' => array('class' => 'col-lg-3', 'style' => 'margin: 1em auto'),
        'itemView' => function ($model) {
            return VideoWidget::widget(['model' => $model]);
        },
        'sorter' => array(
            'class' => LinkSorter::class,
            'attributes' => array('views', 'added'),
        ),
        'pager' => [
            'class' => LinkPager::class,
            'firstPageLabel' => '&laquo;&laquo;',
            'lastPageLabel' => '&raquo;&raquo;',
            'maxButtonCount' => 5,
        ]
    )), ['class' => "video-index"]);