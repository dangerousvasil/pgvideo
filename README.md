# ТыТюбик Project

### INSTALLATION

"ТыТюбик" базируется на 
[yii2-app-basic](https://packagist.org/packages/yiisoft/yii2-app-basic)
и базе данных postgresql.

### Установка via Composer

Композер должен быть установлен в вашей системе, если нет то ниже ссылка на инструкцию.  

> If you do not have [Composer](http://getcomposer.org/), you may install it by following the instructions
at [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).
 
Для дальнейшей работы необходимо собрать проект с помощью команды:

```
composer update  
```

### Запуск контейнеров

Docker должен быть установлен в вашей системе, если нет то ниже ссылка на инструкцию.

> Install Docker on a computer by following the instructions at https://docs.docker.com/get-docker/

Также в системе должен быть установлен пакет "docker-compose" для простой оркестрации контейнеров.

> Install docker-compose on a computer by following the instructions at  https://docs.docker.com/compose/install/

Когда все необходимы инструменты запущены мы можем запустить наш сервис.

```
docker-compose up
```

### База данных

После запуска всех контейнеров необходимо выполнить миграции для наполнения базы данных.
Чтобы правильно сформировать команду надо запомнить имя контейнера который сгенерировал докер для сервиса "php" 
в моём случае это "pgvideo_php_1"

```
docker exec -it pgvideo_php_1 /var/www/yii migrate
```

Контейнер с базой данных настроен так что к нему можно подключиться по порту 15432 под пользователем postgres без пароля

```
postgresql://localhost:15432/postgres
```


### Интерфейс

С помощью бразуера открыть страницу
```
http://127.0.0.1/
```

### Особенности решения
В качестве полей file и thumbnail используется свободно сгенерированный текст в качестве альтернытивы пути к файлу.
- Поле file в данном примере не используется только выполняет роль симуляции файлового пути.
- Поле thumbnail используется в качестве соли для генерации рандомной картинки с помощью сервиса:
[avatars.adorable](http://avatars.adorable.io/#demo).


### Оптимизации
для подсчета количества используется триггер на добавление записей и сохраняться в отдельную таблицу counter, для оптимизации сдвига используется покрывающий индекс
```
Nested Loop  (cost=2222.04..2388.35 rows=20 width=453)
  ->  HashAggregate  (cost=2221.75..2221.95 rows=20 width=8)
        Group Key: video_1.id
        ->  Limit  (cost=2220.78..2221.50 rows=20 width=16)
              ->  Index Only Scan using video_views_id_index on video video_1  (cost=0.42..3556.42 rows=100000 width=16)
  ->  Index Scan using video_pkey on video  (cost=0.29..8.31 rows=1 width=453)
        Index Cond: (id = video_1.id)
```
> если вдруг индекс не используется необходимо выполнить процедуры:
> - analyse;
> - vacuum;


Но при увеличении количества записей данный подход не является оптимальным решением.

### Идея
Более оптимально является использование дополнительных таблиц, которые хранят порядок элементов для каждого типа сортировок, и делать выборки идентификаторов из них опираясь на последний показанный элемент, высчитывая сдвиги относительно него.
```
-- auto-generated definition
create table video_order_by_added
(
    id        bigserial    not null
        constraint video_order_by_added_pkey
            primary key,
    video_id  bigint       not null
);
```
где
- id : порядок эемента в общей выборке с учетом сортировки
- video_id: идентификатор в таблице видео

Зная размер страницы и необходимую страницу мы всегда можем вычислить необходимый элемент по таблице и отталкиваться от него:
```
SELECT * FROM "video" 
    WHERE "id" IN 
        (SELECT "id" FROM "video_order_by_added" WHERE id BETWEEN 10 AND 20);
``` 
В данном случае достаточно тяжело обсуживать таблицы в случае добавления или изменения данных, и в случае добавления новых условий сортировок.

