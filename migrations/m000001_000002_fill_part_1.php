<?php

use app\models\Video;
use yii\db\Migration;

/**
 * Class m000001_000002_fill_part_1
 */
class m000001_000002_fill_part_1 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $faker = Faker\Factory::create();
        // generate random blocks
        for ($i = 0; $i < 100000; $i++) {
            $model = new Video();
            $model->added = $faker->date('Y-m-d H:i:s', 'now');
            $model->title = implode(' ', [$faker->title, $faker->name]);

            $model->thumbnail = $faker->realText(); // like ~~~ url
            $model->file = $faker->realText(); // like ~~~ url

            $model->views = $faker->numberBetween(0, 10000000);
            $model->duration = $faker->numberBetween(3*60, 2*60*60);

            if (!$model->save()) {
                throw new \yii\db\Exception(implode(PHP_EOL, $model->errors));
            }
        }
        // now pg use indexes
        $this->execute('analyse video;');
        $this->execute('vacuum video;');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->truncateTable('video');
    }
}
