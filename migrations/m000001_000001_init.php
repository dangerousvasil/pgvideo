<?php

use app\models\Counter;
use yii\db\Migration;
use yii\db\pgsql\Schema;

/**
 * Class m000001_000001_init
 */
class m000001_000001_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('video', [
            'id' => Schema::TYPE_BIGPK,
            'title' => Schema::TYPE_STRING . ' NOT NULL',
            'file' => Schema::TYPE_STRING . ' NOT NULL',
            'thumbnail' => Schema::TYPE_STRING . ' NOT NULL',
            'added' => Schema::TYPE_DATETIME . ' NOT NULL',
            'duration' => Schema::TYPE_BIGINT . ' NOT NULL',
            'views' => Schema::TYPE_BIGINT . ' NOT NULL',
        ]);
        $this->createIndex('video_views_id_index', 'video', ['views', 'id'], true);
        $this->createIndex('video_added_id_index', 'video', ['added', 'id'], true);

        // create table for count some thing
        $this->createTable('counter', [
            'id' => Schema::TYPE_PK,
            'count' => Schema::TYPE_BIGINT . ' NOT NULL',
        ]);
        // thirst counter for video posts
        $model = new Counter();
        $model->id = Counter::VIDEO_COUNTER;
        $model->count = 0;
        $model->save();

        // trigger for count video blocks
        $this->execute('CREATE OR REPLACE FUNCTION process_video_counter() RETURNS TRIGGER AS
$process_video_counter$
BEGIN
    IF (TG_OP = \'DELETE\') THEN
        UPDATE counter set count=count-1 where id = 1;
    ELSIF (TG_OP = \'INSERT\') THEN
        UPDATE counter set count=count+1 where id = 1;
    END IF;
    RETURN NULL; 
END;
$process_video_counter$ LANGUAGE plpgsql;');
        $this->execute('CREATE TRIGGER trigger_video_counter
    AFTER INSERT OR UPDATE OR DELETE
    ON video
    FOR EACH ROW
EXECUTE PROCEDURE process_video_counter();');

    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->execute('drop function process_video_counter();');
        $this->dropTable('video');
    }


}
