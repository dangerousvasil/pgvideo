<?php

namespace app\providers;


use app\models\Counter;
use app\models\Video;
use yii\base\InvalidConfigException;
use yii\data\ActiveDataProvider;

/**
 * Class VideoDataProvider
 * @package app\providers
 */
class VideoDataProvider extends ActiveDataProvider
{

    /**
     * @var string the column that is used as the key of the data models.
     */
    public $key = 'id';


    /**
     * Optimized the query to use the covering index
     * @return Video[] list of Video blocks for this page
     */
    protected function prepareModels()
    {
        $pagination = $this->getPagination();
        if ($pagination === false) {
            throw new InvalidConfigException('The "pagination" property must be an instance of a class that implements the Pagination');
        }
        $pagination->totalCount = $this->prepareTotalCount();

        $sort = $this->getSort();
        if ($sort === false) {
            throw new InvalidConfigException('The "sort" property must be an instance of a class that implements the Sort');
        }

        return $this->query->where(['in', 'id',
            Video::find()->select(['id'])->limit($pagination->getLimit())->offset($pagination->getOffset())->addOrderBy($sort->getOrders())
        ])->all($this->db);
    }

    /**
     * we know which key we want to use
     * @return array array of key to use
     */
    protected function prepareKeys($models)
    {
        return ['id'];
    }

    /**
     * Optimize pg count by trigger on every row inserted or deleted
     * @return int count video posts
     */
    protected function prepareTotalCount(): int
    {
        return (int)Counter::findOne(Counter::VIDEO_COUNTER)->count;
    }
}
